package com.nikola.revolutandroidtest.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.nikola.revolutandroidtest.R
import timber.log.Timber

class PageViewModel : ViewModel() {

    private val _index = MutableLiveData<Int>()
    val apiBlocked = MutableLiveData<Boolean>(false)
    val text: LiveData<String> = Transformations.map(_index) {
        var message = ""
        Timber.e("int number is %s", it)
        message = if (it == 1) {
            "All possible rates"
        } else {
            "Rates"
        }
        return@map message
    }

    fun setIndex(index: Int) {
        _index.value = index
    }
}