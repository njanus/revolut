package com.nikola.revolutandroidtest.ui.main.network.services


import com.nikola.revolutandroidtest.ui.main.network.ApiConstants
import com.nikola.revolutandroidtest.ui.main.network.response.Rate
import com.nikola.revolutandroidtest.ui.main.network.response.ServerResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiService {


    @GET(ApiConstants.LATEST)
    fun getAllRates(@Query(ApiConstants.BASE) base: String): Single<ServerResponse>

}