package com.nikola.revolutandroidtest.ui.main.listeners

import com.nikola.revolutandroidtest.ui.main.network.response.Rate


interface OnAdapterListener {
    fun onFocusChange(hasFocus: Boolean)
    fun onItemClicked(
        position: Int,
        rate: Rate
    )

    fun finishedTextChanged(value: CharSequence, rate: Rate)
}
