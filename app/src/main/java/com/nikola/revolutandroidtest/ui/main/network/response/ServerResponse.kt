package com.nikola.revolutandroidtest.ui.main.network.response

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class ServerResponse(
    @SerializedName("base") val base: String,
    @SerializedName("date") val date: String,
    @SerializedName("rates") val rates: JsonObject
)
