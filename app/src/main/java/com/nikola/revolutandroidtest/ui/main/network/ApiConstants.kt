package com.nikola.revolutandroidtest.ui.main.network

class ApiConstants {
    companion object {
        const val HTTP_TIMEOUTS: Long = 25000 // 25s
        const val BASE_URL = "https://revolut.duckdns.org/"

        /// whole saved ap call needed
      // https://revolut.duckdns.org/latest?base=EUR

        // HEADERS
        const val HEADER_KEY_CONTENT_TYPE = "Content-Type"
        const val HEADER_VALUE_CONTENT_TYPE_JSON = "application/json"

        // ENDPOINTS
        const val LATEST = "latest"
        const val BASE = "base"

        const val HTTP_OK = 200
        const val DEFAULT_ERROR = 400

    }

}