package com.nikola.revolutandroidtest.ui.main.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_LONG
import com.nikola.revolutandroidtest.R
import com.nikola.revolutandroidtest.ui.main.adapter.RateAdapter
import com.nikola.revolutandroidtest.ui.main.app.AppRepoProvider
import com.nikola.revolutandroidtest.ui.main.app.RateEnum
import com.nikola.revolutandroidtest.ui.main.listeners.OnAdapterListener
import com.nikola.revolutandroidtest.ui.main.network.ApiClient
import com.nikola.revolutandroidtest.ui.main.network.response.Rate
import com.nikola.revolutandroidtest.ui.main.network.response.ServerResponse
import com.nikola.revolutandroidtest.ui.main.network.services.ApiService
import com.nikola.revolutandroidtest.ui.main.viewmodel.PageViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main.*
import timber.log.Timber
import java.util.concurrent.TimeUnit


/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment(),
    OnAdapterListener {
    private var isInit = true
    private var euro = Rate(RateEnum.EUR.toString(), RateEnum.EUR.toString(), "Euro", 100.0)
    private var rates: ArrayList<Rate> = arrayListOf()
    private lateinit var pageViewModel: PageViewModel
    private lateinit var rateDisposable: Disposable
    private lateinit var rateAdapter: RateAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var topRate: Rate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        val textView: TextView = root.findViewById(R.id.section_label)
        recyclerView = root.findViewById(R.id.recyclerViewRates)

        pageViewModel.text.observe(viewLifecycleOwner, Observer<String> {
            if (it.contains(getString(R.string.rates))) {
                rates.add(
                    0, euro
                )
                val name = rates[0].name
                Timber.d("It is not init. $name default")
                getDataFromServer(name)
                textView.text = it
            } else {
                textView.text = it
            }

        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        context?.let { initAdapter(it) }
    }


    private fun initAdapter(context: Context) {

        recyclerViewRates.layoutManager = LinearLayoutManager(context)
        rateAdapter = RateAdapter(context, rates, this)
        recyclerViewRates.adapter = rateAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        rateDisposable.dispose()
    }

    private fun getDataFromServer(baseValue: String) {
        progressBarDetails.visibility = View.VISIBLE
        val service = AppRepoProvider.getAllRates(
            ApiClient.getRetrofit().create(ApiService::class.java), baseValue
        )

        rateDisposable = service
            .repeatWhen { completed -> completed.delay(1, TimeUnit.SECONDS) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { response -> showRateDetails(response) },
                { error -> handleError(error) }
            )


    }

    private fun formatRateModel(response: ServerResponse) {
        rates.clear()
        if (isInit) {
            rates.add(0, euro)
        } else {
            if (this::topRate.isInitialized) {
                rates.add(0, topRate)
            }
        }
        val rateString = response.rates.toString()
        val tempRates: ArrayList<Rate> = arrayListOf()
        val ratesString = rateString.split(",")
        ratesString.forEach {
            val rate = Rate()
            val item = it.replace("{", "")
            val finalItem = item.replace("}", "")
            rate.name = finalItem.split(":")[0].trim()
            rate.value = finalItem.split(":")[1].trim().toDouble()
            tempRates.add(rate)
        }
        rates.addAll(tempRates)
        Timber.d(rates.size.toString())
        if (!pageViewModel.apiBlocked.value!!) {
            rateAdapter.notifyDataSetChanged()
        }
        Timber.d("apiCallIsBlocked value is %s", pageViewModel.apiBlocked.value)
    }

    private fun handleError(error: Throwable?) {
        progressBarDetails.visibility = View.GONE
        Snackbar.make(progressBarDetails, getString(R.string.default_error), LENGTH_LONG)
            .show()
        Timber.e(error)
    }

    private fun showRateDetails(response: ServerResponse) {
        progressBarDetails.visibility = View.GONE
        Timber.d("response from server %s", response.toString())
        formatRateModel(response)
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {

            return PlaceholderFragment()
                .apply {
                    arguments = Bundle().apply {
                        putInt(ARG_SECTION_NUMBER, sectionNumber)
                    }
                }

        }
    }

    override fun onFocusChange(hasFocus: Boolean) {
        if (hasFocus) {
            pageViewModel.apiBlocked.value = true
            Timber.d("onFocusChange has focus")
            rateDisposable.dispose()
        } else {
            pageViewModel.apiBlocked.value = false
            Timber.d("FocusChange no focus")
        }
    }

    override fun onItemClicked(position: Int, rate: Rate) {
        Timber.d("defaultValue $rate.name")
        rateDisposable.dispose()
        rates.removeAt(position)
        isInit = false
        topRate = rate
        rates.add(0, topRate)
        rateAdapter.notifyDataSetChanged()
        recyclerViewRates.layoutManager?.scrollToPosition(0)
        val shortName = topRate.name.replace("\"", "")
        pageViewModel.apiBlocked.value = false
        getDataFromServer(shortName)
    }

    override fun finishedTextChanged(value: CharSequence, rate: Rate) {
        Timber.d("finishedTextChanged fragment")
        Timber.d("finishedTextChanged position $rate")
        rateDisposable.dispose()
        pageViewModel.apiBlocked.value = false
        rates[0].value = value.toString().toDouble()
        getDataFromServer(rate.name.replace("\"", ""))

    }


    override fun onDetach() {
        super.onDetach()
        rateDisposable.dispose()
    }
}