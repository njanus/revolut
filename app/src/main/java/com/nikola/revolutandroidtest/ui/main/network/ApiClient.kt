package com.nikola.revolutandroidtest.ui.main.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiClient {

    @Synchronized
    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(createGsonConverter()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(ApiConstants.BASE_URL)
            .client(createHttpClient())
            .build()
    }

    private fun createGsonConverter(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

    private fun createHttpClient(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()

        // setting timeouts and disabling retries
        clientBuilder.retryOnConnectionFailure(true)
            .readTimeout(ApiConstants.HTTP_TIMEOUTS, TimeUnit.MILLISECONDS)
            .writeTimeout(ApiConstants.HTTP_TIMEOUTS, TimeUnit.MILLISECONDS)
            .connectTimeout(ApiConstants.HTTP_TIMEOUTS, TimeUnit.MILLISECONDS).build()

        clientBuilder.addInterceptor(
            createJsonContentTypeAndHeaderInterceptor()
        )
        clientBuilder.addInterceptor(
            createHttpLoggingInterceptor()
        )
        return clientBuilder.build()
    }

    private fun createHttpLoggingInterceptor(): Interceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    private fun createJsonContentTypeAndHeaderInterceptor(): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()

            val request = original.newBuilder()
                .addHeader(
                    ApiConstants.HEADER_KEY_CONTENT_TYPE,
                    ApiConstants.HEADER_VALUE_CONTENT_TYPE_JSON
                )

                .build()

            chain.proceed(request)
        }
    }
}
