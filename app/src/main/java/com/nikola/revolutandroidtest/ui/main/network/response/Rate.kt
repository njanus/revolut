package com.nikola.revolutandroidtest.ui.main.network.response


data class Rate(
    var imgId: String = "",
    var name: String = "",
    var full_name: String = "",
    var value: Double = 0.0
)

