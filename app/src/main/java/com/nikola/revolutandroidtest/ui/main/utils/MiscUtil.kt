package com.nikola.revolutandroidtest.ui.main.utils

import android.content.Context
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import timber.log.Timber

/** This is class is used for different utils methods */

class MiscUtil {

    companion object {

        fun getApiQueryParameters(date: String): MutableMap<String, String> {
            val parameters: MutableMap<String, String> = hashMapOf()
            Timber.e("Query for api is $parameters")
            return parameters
        }

        fun getProgressLoadingBar(context: Context): CircularProgressDrawable {
            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            return circularProgressDrawable
        }
    }
}