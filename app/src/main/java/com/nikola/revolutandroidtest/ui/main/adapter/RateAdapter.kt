package com.nikola.revolutandroidtest.ui.main.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nikola.revolutandroidtest.R
import com.nikola.revolutandroidtest.ui.main.app.RateEnum
import com.nikola.revolutandroidtest.ui.main.listeners.OnAdapterListener
import com.nikola.revolutandroidtest.ui.main.network.response.Rate
import com.nikola.revolutandroidtest.ui.main.utils.MiscUtil
import timber.log.Timber
import java.math.BigDecimal
import java.math.RoundingMode


class RateAdapter(
    private var context: Context,
    private var rates: MutableList<Rate>,
    private val onAdapterListener: OnAdapterListener
) :
    RecyclerView.Adapter<RateAdapter.RateViewHolderItem>() {
    private var countValue = 1.0
    private lateinit var view: View
    private var deleteTextClicked = false

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): RateViewHolderItem {
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_rate, parent, false)
        return RateViewHolderItem(view)
    }


    inner class RateViewHolderItem internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imageViewIcon: ImageView = itemView.findViewById(R.id.imageViewIcon)
        var editTextValue: EditText = itemView.findViewById(R.id.editTextValue)
        var textViewName: TextView = itemView.findViewById(R.id.textViewName)
        var textViewFullName: TextView = itemView.findViewById(R.id.textViewFullName)
        var relativeLayoutRate: RelativeLayout = itemView.findViewById(R.id.relativeLayoutRate)
    }

    override fun getItemCount(): Int {
        return rates.size
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: RateViewHolderItem, position: Int) {
        val rate: Rate = rates[position]
        val value: Double
        if (position == 0) {
            holder.editTextValue.setText((rate.value).toString())
            countValue = holder.editTextValue.text.toString().toDouble()
        } else {
            value =
                BigDecimal(rate.value * countValue).setScale(2, RoundingMode.HALF_EVEN).toDouble()
            holder.editTextValue.setText((value).toString())
        }

        // Enable click listener
        holder.relativeLayoutRate.setOnClickListener {
            holder.editTextValue.clearFocus()
            this.onAdapterListener.onItemClicked(position, rate)
        }
        holder.editTextValue.setOnKeyListener { v, keyCode, event ->
            deleteTextClicked = keyCode == KeyEvent.KEYCODE_DEL
            false
        }
        // Edit text listeners and focus control
        holder.editTextValue.onFocusChangeListener =
            View.OnFocusChangeListener { _: View, hasFocus: Boolean ->
                if (hasFocus) {
                    this.onAdapterListener.onFocusChange(hasFocus)
                } else {
                    this.onAdapterListener.onFocusChange(false)
                }
            }
        holder.editTextValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Timber.d("afterTextChanged")
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                Timber.d("beforeTextChanged")
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Timber.d("onTextChanged")
                if (position == 0 && s?.length!! > 1 && !deleteTextClicked) {
                    onAdapterListener.finishedTextChanged(s.toString(), rate)
                    holder.editTextValue.clearFocus()
                }
            }
        })
        // Set up country name value
        val shortName = rate.name.replace("\"", "")
        holder.textViewName.text = rate.name.replace("\"", "")
        var fullName = ""
        when (shortName) {
            RateEnum.AUD.toString() -> {
                fullName = "Australian Dollar"
            }
            RateEnum.BGN.toString() -> {
                fullName = "Bulgarian Lev"
            }
            RateEnum.BRL.toString() -> {
                fullName = "Brazilian Real"
            }
            RateEnum.CAD.toString() -> {
                fullName = "Canadian Dollars"
            }
            RateEnum.CHF.toString() -> {
                fullName = "Swiss Franc"
            }
            RateEnum.CNY.toString() -> {
                fullName = "Chinese Yuan"
            }
            RateEnum.CZK.toString() -> {
                fullName = "Czech Koruna"
            }
            RateEnum.DKK.toString() -> {
                fullName = "Danish Krone"
            }
            RateEnum.GBP.toString() -> {
                fullName = "British Pound"
            }
            RateEnum.HKD.toString() -> {
                fullName = "Hong Kong Dollar"
            }
            RateEnum.HRK.toString() -> {
                fullName = "Croatian Kuna"
            }
            RateEnum.HUF.toString() -> {
                fullName = "Hungarian Forint"
            }
            RateEnum.IDR.toString() -> {
                fullName = "Indonesian Rupiah"
            }
            RateEnum.ILS.toString() -> {
                fullName = "Israeli Shekel"
            }
            RateEnum.INR.toString() -> {
                fullName = "Indian Rupee"
            }
            RateEnum.ISK.toString() -> {
                fullName = "Icelandic Krona"
            }
            RateEnum.JPY.toString() -> {
                fullName = "Japanese Yen"
            }
            RateEnum.KRW.toString() -> {
                fullName = "South Korean Won"
            }
            RateEnum.MXN.toString() -> {
                fullName = "Mexican Peso"
            }
            RateEnum.MYR.toString() -> {
                fullName = "Malaysian Ringgit"
            }
            RateEnum.NOK.toString() -> {
                fullName = "Norwegian Krone"
            }
            RateEnum.NZD.toString() -> {
                fullName = "New Zealand Dollar"
            }
            RateEnum.PHP.toString() -> {
                fullName = "Philippine Peso"
            }
            RateEnum.PLN.toString() -> {
                fullName = "Poland Zloty"
            }
            RateEnum.RON.toString() -> {
                fullName = "Romanian Leu"
            }
            RateEnum.RUB.toString() -> {
                fullName = "Russian Ruble"
            }
            RateEnum.SEK.toString() -> {
                fullName = "Swedish Krona"
            }
            RateEnum.SGD.toString() -> {
                fullName = "Singapore Dollar"
            }
            RateEnum.THB.toString() -> {
                fullName = "Thai Baht "
            }
            RateEnum.TRY.toString() -> {
                fullName = "Turkey Lira"
            }
            RateEnum.ZAR.toString() -> {
                fullName = "South African Rand"
            }

            RateEnum.EUR.toString() -> {
                fullName = "Euro"
            }
            RateEnum.USD.toString() -> {
                fullName = "United State Dollar"
            }
        }
        holder.textViewFullName.text = fullName

        /// Set up flag from free API server wit Glide library
        val flag = shortName.substring(0, 2)
        val circularProgressDrawable = MiscUtil.getProgressLoadingBar(context)
        circularProgressDrawable.start()
        Glide.with(context).load("https://www.countryflags.io/$flag/flat/64.png")
            .placeholder(circularProgressDrawable)
            .apply(RequestOptions.circleCropTransform())
            .error(R.mipmap.ic_launcher)
            .centerInside()
            .into(holder.imageViewIcon)
    }
}
