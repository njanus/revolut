package com.nikola.revolutandroidtest.ui.main.app

class DataConstants {
    companion object {
        const val SPLASH_DELAY_TIME: Long = 2500 //2.5 seconds
        const val UPDATE_DATE_TIME: Long = 1000 //1 seconds

    }

}