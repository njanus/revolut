package com.nikola.revolutandroidtest.ui.main.app

import com.nikola.revolutandroidtest.ui.main.network.response.ServerResponse
import com.nikola.revolutandroidtest.ui.main.network.services.ApiService
import io.reactivex.Single


object AppRepoProvider {


    fun getAllRates(service: ApiService, baseValue: String): Single<ServerResponse> {
        return service.getAllRates(baseValue)
    }

}