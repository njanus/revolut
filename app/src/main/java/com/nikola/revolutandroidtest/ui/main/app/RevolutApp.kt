package com.nikola.revolutandroidtest.ui.main.app

import android.app.Application
import com.nikola.revolutandroidtest.BuildConfig
import timber.log.Timber


class RevolutApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }

}